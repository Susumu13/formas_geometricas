#include <iostream>
#include "geometrica.hpp"
#include "triangulo.hpp"

using namespace std;

int main() {
    geometrica *FormaGeometrica1 = new geometrica();
    geometrica FormaGeometrica2(30,20);
    geometrica FormaGeometrica3 = geometrica(40,50);

    triangulo *Triangulo1 = new triangulo();

    cout << "Area FormaGeometrica1 = " << FormaGeometrica1->area() << endl;
    cout << "Area FormaGeometrica2 = " << FormaGeometrica2.area() << endl;
    cout << "Area FormaGeometrica3 = " << FormaGeometrica3.area() << endl;
    cout << "Area Triangulo1 = " << Triangulo1->area() << endl;

}
